#ifndef RD53AMODULE_H
#define RD53AMODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "RD53Emulator/Handler.h"
#include "RD53Emulator/HitTree.h"

#include <TCanvas.h>
#include <string>
#include <thread>
#include <map>
#include <TH1.h>
#include <TH2.h>

/**
 * @brief RD53A ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @date June 2022
 **/

class RD53AModule: public ReadoutModule, public RD53A::Handler{
  
 public:
  
  /**
   * @brief create an RD53A module for MALTA DAQ
   * @param name the module name
   * @param address ipbus connection string. Not relevant.
   * @param outdir the output directory
   **/
  RD53AModule(std::string name, std::string address, std::string outdir);

  /**
   * @brief delete pointers
   **/
  ~RD53AModule();
  
  /**
   * @brief Nothing to do in RD53A
   **/
  void SetInternalTrigger();

  /**
   * Load the connectivity file.
   * Connect to FELIX.
   * Configure the front-end.
   * Create the histograms.
   **/
  void Configure();
  
  /**
   * @brief Start data acquisition thread
   * @param run the run number as a string
   * @param usePath use the path 
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * @brief Stop the data acquisition thread
   **/
  void Stop();

  /**
   * @brief The data acquisition thread
   **/
  void Run();

  /**
   * @brief get the event number from the last hit
   * @return the number of triggers modulo 15
   **/
  uint32_t GetNTriggers();

  /**
   * @brief Has no effect on RD53A
   **/
  void EnableFastSignal();

  /**
   * @brief Could be used to reset the RD53A::Hit event counter
   **/
  void ResetL1Counter();

  /**
   * @brief Has no effect in RD53A
   * @return always true
   **/
  bool RetrieveL1ResetFlag();
  
  /**
   * @brief has no effect on RD53A
   **/
  void DisableFastSignal();

  /**
   * Used to update a canvas that is displayed by MaltaMultiDAQ
   **/
  void PlotCanvasRealTime(TCanvas *c);
  
  ///**
  // * @brief Get the timing histogram
  // * @return The timing histogram
  // **/
  //TH1D* GetTimingHisto();

  ///**
  // * @brief Get the number of hits histogram
  // * @return The number of hits histogram
  // **/
  //TH1D* GetNHitHisto();

  ///**
  // * @brief Get the hit map histogram
  // * @return The hit map histogram
  // **/
  //TH2D* GetHitMapHisto();

 private:
  
  std::map<std::string,TH2I*> m_occ;
  std::map<std::string,TH1I*> m_tot;
  uint32_t m_colMin;
  uint32_t m_colMax;
  uint32_t m_ntrigs;

  RD53A::HitTree* m_hits;

  //TH2D* m_hit_map;
  //TH1D* m_hit_time;
  //TH1D* m_hits_per_event;

};

#endif

