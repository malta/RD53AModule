#include "RD53AModule/RD53AModule.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <unistd.h>
#include "RD53Emulator/Tools.h"

DECLARE_READOUTMODULE(RD53AModule)

using namespace std;
using namespace RD53A;

RD53AModule::RD53AModule(string name, string address, string outdir): 
  ReadoutModule(name,address,outdir), Handler(){  
  
  SetOutPath(outdir);
  SetScan("module");

  m_Hmap =new TH2D((name+"_hitMap").c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
  m_Htime=new TH1D((name+"_timing").c_str(),"timing;BCID since L1A [25 ns]",20,-0.5,19.5); 
  m_Hsize=new TH1D((name+"_nPixel").c_str(),"pixel;# pixel per event",6,-0.5,5.5); 
  m_hits=new HitTree("HitTree" ,"HitTree");

}

RD53AModule::~RD53AModule(){

}

void RD53AModule::Configure(){
  
  if(ReadoutModule::GetConfig()->GetKeyMap().count("mapping")==0){
    cout << m_name << "Missing mapping (connectivity) file" << endl;
    exit(0);
  }

  if(ReadoutModule::GetConfig()->GetKeyMap().count("latency")==0){
    cout << m_name << "Missing latency" << endl;
    exit(0);
  }

  cout << "Load latency" << endl;
  SetLatency(std::stoi(ReadoutModule::GetConfig()->GetKeyMap().at("latency")));

  cout << "Load mapping file" << endl;
  SetMapping(ReadoutModule::GetConfig()->GetKeyMap().at("mapping"), true);
 
  cout << "Connect to FELIX" << endl;
  Connect();

  m_scan_fe = LinFE | DiffFE; //VD hardcoded

  cout << "Configure the Front-end" << endl;
  for(auto fe : GetFEs()){

    cout << "Configure " << fe->GetName() << endl;

    fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::LATENCY_CONFIG,GetLatency());

    if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::NUM_COLS; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
    }else if((m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //synlindiff
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::LIN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(100000,Pixel::Diff));
    }else if((m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){//syn
      m_colMin = Matrix::SYN_COL0; 
      m_colMax = Matrix::SYN_COLN+1; 
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,Configuration::GP_START_ZERO_LEVEL);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1 ,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2 ,0);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->SetGlobalThreshold(Pixel::Lin, 500);
    }else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){//lindiff
      m_colMin = Matrix::LIN_COL0;
      m_colMax = Matrix::NUM_COLS;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
    }else if(!(m_scan_fe & SyncFE) and (m_scan_fe & LinFE) and !(m_scan_fe & DiffFE)){ //lin
      m_colMin = Matrix::LIN_COL0; 
      m_colMax = Matrix::LIN_COLN+1;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);
      fe->SetGlobalThreshold(Pixel::Diff,500);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_DIFF_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC  ,0);
    }else if(!(m_scan_fe & SyncFE) and !(m_scan_fe & LinFE) and (m_scan_fe & DiffFE)){ //Diff
      m_colMin = Matrix::DIF_COL0; 
      m_colMax = Matrix::DIF_COLN+1;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,500);//Tools::chargeToThr(500,Pixel::Sync));
      fe->SetGlobalThreshold(Pixel::Lin, 500);//Tools::chargeToThr(500,Pixel::Lin));
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
      fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);
    }else{
      m_colMin = Matrix::LIN_COL0;
      m_colMax = Matrix::NUM_COLS;
      fe->GetConfig()->SetField(Configuration::GLOBAL_PULSE_RT,0);
      fe->SetGlobalThreshold(Pixel::Sync,Tools::chargeToThr(500,Pixel::Sync));
    }

    fe->WriteGlobal();
    Send(fe);

    if (ReadoutModule::GetConfig()->GetKeyMap().at("masking")=="true"){
      fe->ApplyMasking();
    } else {
      fe->EnableAll();
    }  
    ConfigurePixels(fe);
 
    //Create histograms
    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]=new TH1I(("mask_"+fe->GetName()).c_str(),"ToT", 16, -0.5, 15.5);

  }

  GetMasker()->SetVerbose(Handler::m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,192);
  GetMasker()->SetShape(m_colMax-m_colMin,192);
  GetMasker()->Build();
  GetMasker()->GetStep(0);

  for(auto fe : GetFEs()){
    for(auto cc : GetMasker()->GetCoreColumns()){
      fe->EnableCoreColumn(cc,true);
      fe->EnableCalCoreColumn(cc,true);
    }
    for(auto dc : GetMasker()->GetDoubleColumns()){
      fe->EnableCalDoubleColumn(dc,true);
    }
    fe->ProcessCommands();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }
  
}

void RD53AModule::Start(string run, bool usePath){
  string fname;
  cout << m_name << ": Start run " << run << endl;

  if(!usePath){
    ostringstream os;
    os << m_outdir << "/run_"  << run << ".root";
    fname = os.str();
  }else{
    fname = run;
  }
  
  m_rootfile=TFile::Open(fname.c_str(),"RECREATE");
  
  cout << m_name << ": Create data acquisition thread" << endl;
  m_cont=true;
  m_thread = thread(&RD53AModule::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;
   
}

void RD53AModule::Run(){

  uint32_t prev_evnum=0;
  uint32_t nhits_total = 0;

  while(m_cont){

    uint32_t nhits = 0;

    do {
      nhits = 0;
      for(auto fe : GetFEs()){
        if(!fe->HasHits()) continue;
        Hit* hit = fe->GetHit();
	if(hit!=0 && hit->GetTOT()!=0x0){
	  nhits++;
	  nhits_total++;
	  m_hits->Fill(hit);
	  m_tot[fe->GetName()]->Fill(hit->GetTOT());
          m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
          m_Hmap ->Fill(hit->GetCol(),hit->GetRow());
	  m_Htime->Fill(hit->GetBCID());
	  m_ntrigs=hit->GetEvNum();
	  if(hit->GetEvNum()!=prev_evnum){
	    m_Hsize->Fill(nhits_total);
	    for (int c=0; c<(hit->GetEvNum()-prev_evnum-1); c++) m_Hsize->Fill(0.);
	    prev_evnum=hit->GetEvNum();
	    nhits=0;
	    nhits_total=0;
	  }
	}
        fe->NextHit();
      }      

    }while (nhits > 0);
    
  }
  
}

void RD53AModule::Stop(){
  cout << "Disconnect from FELIX" << endl;
  Disconnect();  
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  m_thread.join();
  cout << m_name << ": Join data acquisition thread" << endl;
  m_running=false;
  cout << "Writing output into : " << m_rootfile->GetName() << endl;
  m_rootfile->WriteObject(m_Htime,"Timing");
  m_rootfile->WriteObject(m_Hsize,"NHit"  );
  m_rootfile->WriteObject(m_Hmap ,"HitMap");
  m_rootfile->cd();
  m_hits->Write();
  cout << m_name << ": Close tree" << endl;
  m_rootfile->Close();
}

uint32_t RD53AModule::GetNTriggers(){
  return m_ntrigs;
}

void RD53AModule::EnableFastSignal(){}

void RD53AModule::DisableFastSignal(){}

void RD53AModule::PlotCanvasRealTime(TCanvas *c){}

void RD53AModule::ResetL1Counter(){}

//TH1D* RD53AModule::GetTimingHisto(){
//  return m_hit_time;
//}

//TH1D* RD53AModule::GetNHitHisto(){
//  return m_hits_per_event;
//}

//TH2D* RD53AModule::GetHitMapHisto(){
//  cout << "RETURNING POINTER: " << m_hit_map << endl;
//  cout << " with entries: " << m_hit_map->GetEntries() << endl;
//  return m_hit_map;
//}


bool RD53AModule::RetrieveL1ResetFlag(){return true;}
